package com.acutex;

import com.acutex.connection.WebUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 */

@Path("/getList")
public class FormList {

    String formListLink = "http://localhost:8888/formList";


    @GET
    @Produces("text/plain")
    public String uploadForm() {

       String data =   WebUtil.getFormList(formListLink);


        return data;
    }



}
