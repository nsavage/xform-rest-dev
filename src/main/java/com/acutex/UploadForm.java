
package com.acutex;

import com.acutex.connection.WebUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.File;

// The Java class will be hosted at the URI path "/UploadForm"
@Path("/upload")
public class UploadForm {

    //    /home/ubu/xform_server_aggregate/sample-forms/Birds.xml
    //  /home/ubu/nigeria/nigeria.xml

    String formUploadLink = "http://localhost:8888/formUpload";
    String filePath = "/home/ubu/nigeria/nigeria.xml";

    @GET
    @Produces("text/plain")
    public String uploadForm() {

        String data  = WebUtil.testServerConnectionWithHeadRequest(formUploadLink);
        System.out.println("testServerConnectionWithHeadRequest returns "+data);

        File formFile = new File(filePath);
        System.out.println("found file of size: "+formFile.length());

        boolean result = WebUtil.uploadFormToServer(formFile, formUploadLink);

        return "Server returns: "+result;

    }
}
