package com.acutex;

import com.acutex.connection.WebUtil;
import com.acutex.constants.HtmlConsts;
import com.acutex.constants.HtmlStrUtil;
import com.acutex.constants.HtmlUtil;
import com.acutex.constants.ServletConsts;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.File;
import java.io.IOException;

/**
 *  uploads a submission
 */

@Path("/uploadSub")
public class UpLoadSubmission {


    //    /home/ubu/xform_server_aggregate/sample-forms/Birds.xml
    //  /home/ubu/nigeria/nigeria.xml

    String formUploadLink = "http://localhost:8888/submission";
    String filePath = "/home/ubu/nigeria/instances/sub1/submission.xml";


    @GET
    @Produces("text/html")
    public String upLoadSubmission() {

 /*       int statusCode = WebUtil.testServerConnectionWithHeadRequest();
        System.out.println("testServerConnectionWithHeadRequest returns "+statusCode);
        File formFile = new File(filePath);
        System.out.println("found file of size: "+formFile.length());
        boolean result = WebUtil.uploadFormToServer(formFile, formUploadLink); */





        return buildUploadForm();




    }

    private String buildUploadForm() {

        StringBuilder headerString = new StringBuilder();
        headerString.append("<script type=\"application/javascript\" src=\"");
        headerString.append("/"+ServletConsts.UPLOAD_SCRIPT_RESOURCE);
        headerString.append("\"></script>");
        headerString.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
        headerString.append("/"+ServletConsts.UPLOAD_STYLE_RESOURCE);
        headerString.append("\" />");
        headerString.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
        headerString.append("/"+ServletConsts.UPLOAD_BUTTON_STYLE_RESOURCE);
        headerString.append("\" />");
        headerString.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
        headerString.append("/"+ServletConsts.AGGREGATE_STYLE);
        headerString.append("\" />");




        StringBuilder htmlString = new StringBuilder();
        htmlString.append(HtmlConsts.HTML_OPEN);
        htmlString.append(HtmlStrUtil.wrapWithHtmlTags(HtmlConsts.HEAD, headerString.toString()+HtmlStrUtil.wrapWithHtmlTags(
                HtmlConsts.TITLE, "upload submission form")));
        htmlString.append(HtmlConsts.BODY_OPEN);
        htmlString.append(UPLOAD_PAGE_BODY_START);
        htmlString.append(UPLOAD_PAGE_BODY_MIDDLE);
        htmlString.append(HtmlConsts.BODY_CLOSE + HtmlConsts.HTML_CLOSE);
        return  htmlString.toString();
    }




    private static final String UPLOAD_PAGE_BODY_START =

            "<div style=\"overflow: auto;\">"
                    + "<p id=\"subHeading\"><b>Upload one submission into ODK Aggregate</b></p>"
                    + "<!--[if true]><p style=\"color: red;\">For a better user experience, use Chrome, Firefox or Safari</p>"
                    + "<![endif] -->"
                    + "<form id=\"ie_backward_compatible_form\""
                    + "                        accept-charset=\"UTF-8\" method=\"POST\" encoding=\"multipart/form-data\" enctype=\"multipart/form-data\""
                    + "                        action=\"";// emit the ADDR



    private static final String UPLOAD_PAGE_BODY_MIDDLE = "\">"
            + "    <table id=\"uploadTable\">"
            + "     <tr>"
            + "        <td><label for=\"xml_submission_file\">Submission data file:</label></td>"
            + "        <td><input id=\"xml_submission_file\" type=\"file\" size=\"80\" class=\"gwt-Button\""
            + "           name=\"xml_submission_file\" /></td>"
            + "     </tr>"
            + "     <tr>"
            + "        <td><input type=\"submit\" name=\"button\" class=\"gwt-Button\" value=\"Upload Submission\" /></td>"
            + "        <td />"
            + "     </tr>"
            + "    </table></form></div>";



}
